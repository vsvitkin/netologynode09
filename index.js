/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 8
*/

const mongo = require('mongodb');

const client = mongo.MongoClient;
const url = 'mongodb://localhost:27017/vsvitkin';
console.log(`Connecting to ${url}...`)

client.connect(url, function (err, db){
  if (err) {
    console.log(`Can not connect to '${url}'. Error text: ${err}`);
  } else {
    work(db);
  }
});

function work(db){
  const collection = db.collection('names');
  console.log('Original database.');
  showUsers(collection,()=>{
    insertUsers(collection,()=>{
      showUsers(collection,()=>{
        updateUsers('Vasily', 'Sergei', collection, ()=>{
          updateUsers('Anatoly', 'Maxim', collection, ()=>{
            showUsers(collection,()=>{
              removeUser('Maxim',collection, ()=>{
                showUsers(collection,()=>{
                  final(db, collection);
                });
              });
            });
          });
        });
      });
    });
  });
}

function final(db,collection){
  console.log('Clearing collection...');
  collection.remove();
  db.close();
}

function insertUsers(collection, callback){
    const user1={'name':'Anatoly'};
    const user2={'name':'Vasily'};
    const user3={'name':'Eugene'};
    console.log(`Adding users: ${user1.name}, ${user2.name}, ${user3.name}`);
    collection.insert([user1, user2, user3], (err,res)=>{
      callback();
    });

}

function removeUser(name, collection, callback){
  console.log(`Deleteing ${name}`);
  collection.remove({'name':name}, (err,res)=>{
    callback();
  });
}

function updateUsers(from, to, collection, callback){
  console.log(`Updating ${from}->${to}`);
  collection.update({'name':from},{'name':to}, (err,res)=>{
    callback();
  });
}

function showUsers(collection, callback){
  collection.find().toArray((err,res)=>{
    showRes(err,res,callback);
  });
}

function showRes(err,res, callback){
  if (err){
    console.log(`Error: ${err}`);
  } else {
    console.log('-------------------------------------------------------');
    if (res.length==0){
      console.log('User list is empty.');
    } else {
    console.log('User list:')
    for (user of res){
      console.log(user.name);
    }
  }
  }
  callback();
}
